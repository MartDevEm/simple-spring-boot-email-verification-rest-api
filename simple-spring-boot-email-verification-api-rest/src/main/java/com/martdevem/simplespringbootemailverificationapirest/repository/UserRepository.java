package com.martdevem.simplespringbootemailverificationapirest.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.martdevem.simplespringbootemailverificationapirest.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String>{
	User findByEmailIdIgnoreCase(String emailId);	

}
