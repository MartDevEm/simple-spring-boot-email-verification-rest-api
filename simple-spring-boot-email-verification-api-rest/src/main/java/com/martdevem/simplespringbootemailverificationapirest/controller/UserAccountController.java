package com.martdevem.simplespringbootemailverificationapirest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.martdevem.simplespringbootemailverificationapirest.model.ConfirmationToken;
import com.martdevem.simplespringbootemailverificationapirest.model.User;
import com.martdevem.simplespringbootemailverificationapirest.repository.ConfirmationTokenRepository;
import com.martdevem.simplespringbootemailverificationapirest.repository.UserRepository;
import com.martdevem.simplespringbootemailverificationapirest.service.EmailSenderService;

@RestController
public class UserAccountController {
	
	@Autowired 
	private UserRepository userRepository;
	
	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;
	
	@Autowired
	private EmailSenderService emailSenderService;
	
	@RequestMapping(value="/register", method = RequestMethod.GET)
	public ModelAndView displayRegistration(ModelAndView modelAndView, User user) {
		
		modelAndView.addObject("user", user);
		modelAndView.setViewName("register");
		return modelAndView;
	}
	

	@RequestMapping(value="/register", method = RequestMethod.POST)
	public ModelAndView registerUser(ModelAndView modelAndView, User user) {
		
		User existingUser = userRepository.findByEmailIdIgnoreCase(user.getEmailId());
		if(existingUser != null) {
			modelAndView.addObject("message","This e-mail already exists!");
			modelAndView.setViewName("error");
		}
		
		else{
			
			this.userRepository.save(user);
			
			ConfirmationToken confirmationToken = new ConfirmationToken(user);
			
			this.confirmationTokenRepository.save(confirmationToken);
			
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			System.out.println("AAAAA");
			mailMessage.setTo(user.getEmailId());
			mailMessage.setSubject("Complete Registration!");
			mailMessage.setFrom("js2590651@gmail.com");
			mailMessage.setText("To confirm your account, click here: "
					+"http://localhost:8082/confirm-account?token="+confirmationToken.getConfirmationToken());//In the purpose of test environment, like running in localhost, and using http, the confirmation link should use http too otherwise you will get an error.
			
			this.emailSenderService.sendEmail(mailMessage);
			
			modelAndView.addObject("emailId", user.getEmailId());
			
			modelAndView.setViewName("successfulRegistration");
			
			
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView confirmUserAccount(ModelAndView modelAndView, @RequestParam("token")String confirmationToken) {
		
		ConfirmationToken token = this.confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		
		if(token != null) {
			User user = userRepository.findByEmailIdIgnoreCase(token.getUser().getEmailId());
			user.setEnabled(true);
			userRepository.save(user);
			modelAndView.setViewName("accountVerified");
		}
		else {
			modelAndView.addObject("message", "The link is invalid or broken!");
			modelAndView.setViewName("error");
		}
		
		return modelAndView;
	}


	public UserRepository getUserRepository() {
		return userRepository;
	}


	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}


	public ConfirmationTokenRepository getConfirmationTokenRepository() {
		return confirmationTokenRepository;
	}


	public void setConfirmationTokenRepository(ConfirmationTokenRepository confirmationTokenRepository) {
		this.confirmationTokenRepository = confirmationTokenRepository;
	}


	public EmailSenderService getEmailSenderService() {
		return emailSenderService;
	}


	public void setEmailSenderService(EmailSenderService emailSenderService) {
		this.emailSenderService = emailSenderService;
	}
	

	
}
