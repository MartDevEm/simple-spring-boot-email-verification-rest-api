package com.martdevem.simplespringbootemailverificationapirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {
	
	private JavaMailSender javaMailsender;
	
	@Autowired
	public EmailSenderService(JavaMailSender javaMailSender) {
		this.javaMailsender = javaMailSender;
	}
	
	@Async
	public void sendEmail(SimpleMailMessage email) {
		this.javaMailsender.send(email);
	}
	

}
