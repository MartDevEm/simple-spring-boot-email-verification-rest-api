package com.martdevem.simplespringbootemailverificationapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSpringBootEmailVerificationApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleSpringBootEmailVerificationApiRestApplication.class, args);
	}

}
